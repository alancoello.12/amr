# Autonomous Mobile Robot - CoviBot

Este proyecto se trata sobre la construcción de un robot autónomo. Hablando de autonomía, este es capaz de moverse y funcionar en general sin la necesidad de supervisión humana.

La arquitectura del robot se basa por tener en esta una computadora Jetson Nano que trabaja en conjunto con una computadora externa. Con ello quiero decir que las Jetson y la computadora deben estar comunicadas para funcionar.

La Jetson se usa principalmente para hacer lectura de los sensores y transmitirlos y también hacer procesamiento de imágenes. Mientras que la computadora externa se encarga de procesar el resto de datos. De esta forma tenemos una red de ROS, donde el ROS Master se ejecuta en una computadora externa.

## Conectarse a Jetson inicialmente

Para poder conectarse a la Jetson Nano y poder instalar paquetes y correr comandos en esta, seguir los siguientes pasos:

1. Tener disponible o crear una red wifi con ssid `covibot` y clave `covibot1`. Esta red se puede crear incluso con hotspot de celular. La jetson nano esta configurada para conectarse a una red con estas características de manera predeterminada.
2. Conectar la computadora master externa a la red.
3. Verificar la ip de la jetson nano en la red wifi.
4. Correr el siguiente comando en una terminal:

   ```bash
   ssh sasilva@<ip_jetson>
   ```

   **La contraseña es: 12345678**

   **Nota: si se obtiene algún error de falta de un paquete de ssh, instalar con `sudo apt install openssh*`**

De esta forma podrán conectarse a la Jetson Nano, que será necesario para que la vinculen con un host de husarnet y puedan luego transmitir información por internet.

## Prerequisitos

Es importante conocer que la Jetson Nano dentro del robot ya tiene un sistema operativo basado en Ubuntu18. Se debe incluir los paquetes de este repositorio o el repositorio en si mismo tanto en la Jetson como computadora externa. Y se debe de seguir el mismo procedimiento en ambas, como la instalación de paquetes.

Para poder usar el robot, entre estos los más básicos son el conocimiento de linux y ROS, además de su instalación. Si se desea primero aprender un poco de esto, pueden seguir los tutoriales guía de [Curso Intro a ROS](https://github.com/FunPythonEC/curso_intro_ros).

Antes de avanzar con el uso del robot, se requiere realizar la instalación de otros programas como husarnet, paquetes de ros y descargar el repositorio, esto se detalla brevemente a continuación:

### Husarnet

Para la comunicación entre la computadora y la jetson nano, se hace el uso de [Husarnet](https://husarnet.com/).

Para la instalación de husarnet, seguir la siguiente guía [install husarnet](https://husarnet.com/docs/manual-client). Esta debe ser instalada tanto en la Jetson y en la computadora master.

Se deben de conectar la computadora y jetson al mismo network usando el comando:

```bash
husarnet join \
fc94:b01d:1803:8dd8:b293:5c7d:7639:932a/hU3yTi3UGrcxg2JroMWmNk \
mydevice
```

Después de unirse con la jetson, se puede conectar por ssh por internet usando:

```bash
ssh sasilva@mydevice
```

Cuando los dispositivos estén conectados, deberían aparecer algo así con el status en `online`. Además configurar a la computadora externa como la `ROS master`.
![husarnet_devices](/media/husarnet_devices.png)

### Paquetes de ROS

Existen algunos paquetes y tipo de dependencias que pueden faltar para poder correr el proyecto. Si se presenta algún problema frente a esto y falta algo de instalar, revisar el error del paquete necesitado e instalarlo con:

```bash
sudo apt install ros-melodic-<nombre_paquete>
```

A continuación pongo un comando para instalar algunos de los paquetes necesitados.

```bash
sudo apt update
sudo apt install ros-melodic-turtlebot3* ros-melodic-ros-control ros-melodic-ros-controllers ros-melodic-dwa* ros-melodic-laser-filters ros-melodic-leg-detector ros-melodic-cartographer* ros-melodic-ompl* ros-melodic-octomap* ros-melodic-fcl* libfcl-dev
```

Para tener el modelo del turtlebot3 predeterminado configurado, correr lo siguiente:

```bash
echo "export TURTLEBOT3_MODEL=burger" >> ~/.bashrc
source ~/.bashrc
```

Esto ultimo es necesario para poder usar ciertos paquetes del turtlebot3 sin tener que declarar el modelo en cada momento.

### Realsense drivers

Para poder utilizar las cámaras de intel, es necesario instalar los drivers de ROS de estas, para ello se puede seguir la guía de instalación en [realsense-ros](https://github.com/IntelRealSense/realsense-ros)

### Preparando repositorio

Se debe de clonar este repositorio e instalar un par de cosas adicionales, a continuación se detalla paso a paso:

1. Clonar el repositorio usando `git clone <link_repo>`.

2. Correr los siguientes comando:

   ```bash
   cd ./amr
   cp -avr ./entorno_sim ~/.gazebo/models
   cd ./amr_ws
   catkin_make
   ```

3. Se deben de agregar submodulos y paquetes de otros repositorios, usar:

   ```bash
   git submodule init
   git submodule update
   ```

4. Se debe de instalar darknet_ros como se ve en su [repositorio](https://github.com/leggedrobotics/darknet_ros)

5. Terminando la instalación de darknet_ros, ejecutar `catkin_make` en el espacio de trabajo principal.

6. Para no tener que hacer source al workspace a cada momento, correr el siguiente comando y luego usar terminales nuevas:

   ```bash
   echo "source ~/amr/amr_ws" >> ~/.bashrc
   source ~/.bashrc
   ```

Una vez instalado lo de este repositorio, los paquetes de ROS, realsense y configurado husarnet en la jetson y computadora master, se puede correr algunos demos. Para poder probar el robot tanto en simulación y experimentalmente, referirse a la guía de [demos](DEMOS.md).

### **Nota: Si se presenta algún problema y no se puede resolver o se tiene alguna duda, contactar al correo sasilva1998@gmail.com**
